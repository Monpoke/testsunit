package iut.tdd;

public class Convert {

    public static String num2text(String input) {

        String all = "";
        int nb = convertInt(input);

        /**
         * on va diviser par nombre de chiffre
         */
        do {
            String conv = "";

            if (nb < 10) {
                conv += " " + convertChiffre(nb);
                nb /= 10;
            } else if (nb < 100) {
                conv += " " + convertDizaines(nb);
                nb /= 100;
            } else if (nb < 1000) {
                conv += " " + convertCentaines(nb);
                nb /= 1000;
            } else {
                conv += " " + nb;
                nb /= 10;
            }

            all += conv;
        } while (nb > 0);

        return all.substring(1);
    }

    public static String text2num(String input) {
        return null;
    }

    public static int convertInt(String text) {
        int c = 0;

        try {
            c = Integer.parseInt(text);
        } catch (Exception e) {

        }

        return c;
    }

    public static void main(String[] args) {
        System.out.println(num2text("0"));
        System.out.println(num2text("1"));
        System.out.println(num2text("100"));
        System.out.println(num2text("101"));
        System.out.println(num2text("111"));
        System.out.println(num2text("578"));
        System.out.println(num2text("300"));
        System.out.println(num2text("302"));
        System.out.println(num2text("801"));
    }

    private static String convertChiffre(int last) {
        switch (last) {
            case 0:
                return "zéro";
            case 1:
                return "un";
            case 2:
                return "deux";
            case 3:
                return "trois";
            case 4:
                return "quatre";
            case 5:
                return "cinq";
            case 6:
                return "six";
            case 7:
                return "sept";
            case 8:
                return "huit";
            case 9:
                return "neuf";
            default:
                return "erreur";
        }
    }

    private static String convertDizaines(int dizaine) {

        /**
         * Cas particuliers 10 11 12 13 14 15 16
         *
         * 20 30 40 50 60 80 90
         */
        boolean dizaineSpeciale = false;

        // cas particulier
        int x = (dizaine) - (dizaine % 10); // on obtient la dizaine

        if (Constantes.casSeuls.containsKey(dizaine)) {
            dizaineSpeciale = true;
            return Constantes.casParticuliers.get(dizaine);
        } // on vérifie les composées
        else if (Constantes.dizainesComposees.containsKey(x)) {
            int sousDizaineSpeciale = Constantes.dizainesComposees.get(x);
            int sousDizaine = dizaine - sousDizaineSpeciale;

            String o = convertDizaines(sousDizaineSpeciale);
            String h = convertDizaines(sousDizaine);

            if (o.equals("cent") && h.equals("onze")) {
                o += " et " + h;
            } else {
                o += "-" + h;
            }

            return o;
        }

        // la dizaine, et on ajoute le dernier chiffre
        String re = "";
        if (Constantes.casParticuliers.containsKey(x)) {
            re += Constantes.casParticuliers.get(x);
        }

        int dernierChiffre = dizaine % 10;
        if (dernierChiffre > 0) {

            if (dernierChiffre == 1) {

                if (re.length() > 0) {
                    re += "-";
                }
                re += "et";
            }

            if (dernierChiffre != 1) {
                if (re.length() > 0) {
                    re += "-";
                }
                re += "";
            } else {
                re += " ";
            }

            re += convertChiffre(dernierChiffre);
        }

        return re;

    }

    private static String convertCentaines(int centaine) {

        int x = (centaine) - (centaine % 100); // on obtient la dizaine
        String o = "";

        if (x > 100) {
            o += convertChiffre(x / 100) + "-";
        }

        o += "cent";

        String dizaines = "";
        
        if (centaine != x) {
            dizaines = convertDizaines(centaine - x);
            
            if(dizaines.length() > 2 && dizaines.substring(0, 2).equals("et")){
                dizaines = " " + dizaines;
            } else {
                dizaines = "-" + dizaines;
            }
            
        } else {
            dizaines = (centaine > x) ? "s" : "";
        }

        
        
        if (x > 100 && dizaines.length() == 0) {
            o += "s";
        } else {
            o += dizaines;
        }

        return o;
    }

}
