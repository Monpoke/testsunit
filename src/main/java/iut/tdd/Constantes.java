/*
 *  Pierre Bourgeois - 2014
 */
package iut.tdd;

import java.util.HashMap;

/**
 *
 * @author Pierre
 */
public abstract class Constantes {
    public final static HashMap<Integer, String> casParticuliers = new HashMap<Integer, String>();
    public final static HashMap<Integer, Boolean> casSeuls = new HashMap<Integer, Boolean>();
    public final static HashMap<Integer, Integer> dizainesComposees = new HashMap<Integer, Integer>();
    
    static {
        /**
         * Cas particuliers
         */
        casParticuliers.put(10, "dix");
        casParticuliers.put(11, "onze");
        casParticuliers.put(12, "douze");
        casParticuliers.put(13, "treize");
        casParticuliers.put(14, "quatorze");
        casParticuliers.put(15, "quinze");
        casParticuliers.put(16, "seize");
        casParticuliers.put(20, "vingt");
        casParticuliers.put(30, "trente");
        casParticuliers.put(40, "quarante");
        casParticuliers.put(50, "cinquante");
        casParticuliers.put(60, "soixante");
        casParticuliers.put(70, "soixante-dix");
        casParticuliers.put(80, "quatre-vingt");
        casParticuliers.put(90, "quatre-vingt-dix");
        casParticuliers.put(100, "cent");
        
        
        /**
         * Cas seuls
         */
        casSeuls.put(11,true);
        casSeuls.put(12,true);
        casSeuls.put(13,true);
        casSeuls.put(14,true);
        casSeuls.put(15,true);
        casSeuls.put(16,true);
        
        /**
         * Cas dizaines composées
         */
        dizainesComposees.put(70,60);
        dizainesComposees.put(90,80);
        
    }
}
